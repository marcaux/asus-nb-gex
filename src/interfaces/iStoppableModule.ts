export interface IStoppableModule {
    start(): void;
    stop(): void;
}
